async function unWreckIt(token) {
    const isDead = token.document.getFlag('csm-lancer-qol', 'isDead');
    if (isDead) {
        console.log(`${token.name} is back from the dead!`);
        const originalImgPath = token.document.getFlag('csm-lancer-qol', 'originalImgPath');
        const updates = [{
            _id: token.document._id,
            texture: {
                src: originalImgPath
            },
            flags: {
                'csm-lancer-qol': {
                    isDead: false
                }
            }
        }];
        await canvas.scene.updateEmbeddedDocuments("Token", updates);
    } else {
        console.log(`${token.name} is already unwrecked.`);
    }
    return token;
}


let i = 0;
target = null;
while (i < arguments.length) {
    if (arguments[i] != undefined) {
        if (arguments[i].constructor.name == "LancerToken") {
            target = arguments[i];
            await unWreckIt(target);
        } else if (arguments[i].constructor.name == "LancerTokenDocument") {
            target = arguments[i]._object;
            await unWreckIt(target);
        }
    }
    i++;
}
