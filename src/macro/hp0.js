  // This is a simple script to set HP to 0 without having to
  // open the sheet.
  if (canvas.tokens.controlled.length !== 1) {
	ui.notifications.error("Select one and only one token.");
	return;
}
await actor.update({"system.hp.value": 0});
