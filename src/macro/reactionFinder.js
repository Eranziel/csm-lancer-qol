let itemMap = await actor.items.contents

//console.log(itemMap)

//Sanitize. Removing duplicate entries.
let itemNames = []
for (let i = 0; i < itemMap.length; i++) {
    let itemName = itemMap[i].name
    if (!itemNames.includes(itemName)){
        itemNames.push(itemName)
    } else {
        itemNames.push("dupe")
    }
}

//console.log(itemNames)

let uniqItemMap = []
for (let i = 0; i < itemMap.length; i++) {
    if (itemNames[i] == "dupe") {
        continue;
    }
    uniqItemMap.push(itemMap[i])
}

console.log(uniqItemMap)

const findReaction = async (item) => {
    for (let key in item) {
        if (Array.isArray(item[key])) {
            for (let i = 0; i < item[key].length; i++) {
                const result = await findReaction(item[key][i]);
                if (result) return result;
            }
        } else if(typeof(item[key]) === 'object') {
            const result = await findReaction(item[key]);
            if (result) return result;
        } else if (key === 'activation' && item[key] === 'Reaction') {
            return item;
        }
    }
    return null;
}



const foundReactions = uniqItemMap.reduce((acc,cur) => {
    findReaction(cur).then(result => {
        if (result) acc.push(result);
    })
    return acc;
}, []);

console.log(foundReactions)