import { log } from "./log.js";

function setFlaggedEffect(targetID, effect, duration, note, originID) {
    const statusEffect = CONFIG.statusEffects.find(x => x.name === effect);
    const target = canvas.tokens.placeables.find(x => x.id === targetID);
    let effectData = {
        name: statusEffect.name,
        icon: statusEffect.icon,
        flags: {
            'core': {
                statusEffectsEnabled: true,
                statusId: statusEffect.id
            },
            'csm-lancer-qol': {
                targetID: targetID,
                effect: statusEffect.name,
                duration: duration,
                note: note,
                originID: originID,
                appliedRound: game.combat.round
            }
        },
        changes: []
    };
    target.actor.createEmbeddedDocuments("ActiveEffect", [effectData]);
}

export function setTimedEffect(token) {
    if (!token) return ui.notifications.error(`Token not found!`);
    if (!game.combat) return ui.notifications.error('You are not in combat!');

    let durations = [
        {
            label: 'end of targets next turn',
            turns: 1,
            rounds: 0
        },
        {
            label: 'start of targets next turn',
            turns: 1,
            rounds: 0
        },
        {
            label: 'end of originators next turn',
            turns: 1,
            rounds: 0
        },
        {
            label: 'start of originators next turn',
            turns: 1,
            rounds: 0
        }
    ]

    new Dialog({
        title: "Set a Timed Effect",
        content: `
      <form>
        <div class="form-group">
          <label>Target:</label>
          <select id="targetID" name="targetID">
            ${canvas.tokens.placeables.map(n => `<option selected="${token.id}" value="${n.id}">${n.name}</option>`)}
          </select>
        </div>
        <div class="form-group">
          <label>Condition:</label>
          <select id="effect" name="effect">
            ${CONFIG.statusEffects.map(o => `<option value="${o.name}">${o.name}</option>`)}
          </select>
        </div>
        <div class="form-group">
          <label>Duration:</label>
          <select id="duration" name="duration">
            ${durations.map(p => `<option value="${p.label}">${p.label}</option>`)}
          </select>
        </div>
        <div class="form-group">
          <label>Note:</label>
          <input id="note" name="note" type="text" value="">
        </div>
        <div class="form-group">
          <label>Originator:</label>
          <select id="originID" name="originID">
            ${canvas.tokens.placeables.map(n => `<option selected="${token.id}" value="${n.id}">${n.name}</option>`)}
          </select>
        </div>
      </form>
    `,
        buttons: {
            ok: {
                label: "OK",
                callback: async (html) => {
                    let targetID = html.find('[name=targetID')[0].value;
                    let effect = html.find('[name=effect]')[0].value;
                    let duration = durations.find(x => x.label === html.find('[name=duration')[0].value);
                    let note = html.find('[name=note]')[0].value;
                    let originID = "";
                    // If this is being tracked relative to the targets turns
                    // we need the origin to match them
                    if (duration.label.includes('targets')) {
                        originID = html.find('[name=targetID')[0].value;
                    } else {
                        originID = html.find('[name=originID')[0].value;
                    }
                    log(`TargetID selected: ${targetID}`);
                    log(`Effect selected: ${effect}`);
                    log(`Duration selected: ${duration.label} Turns: ${duration.turns} Rounds: ${duration.rounds}`);
                    log(`Note entered: ${note}`);
                    log(`OriginID selected: ${originID}`);
                    setFlaggedEffect(targetID, effect, duration, note, originID);
                }
            },
            cancel: {
                label: "Cancel"
            }
        }
    }).render(true);
}

function effectsReport(token) {
    log('**effectsReport**');
    const effects = token.actor.effects.filter(x => x.disabled === false);
    console.log(effects);
    if (effects.length > 0) {
        let html = `<h3>Effects on ${token.name}</h3>`;
        html += '<ul>';
        for (let m = 0; m < effects.length; m++) {
            let note = effects[m].getFlag('csm-lancer-qol', 'note');
            if (note) {
                html += `<li>${effects[m].name} - ${note}`;
            } else {
                html += `<li>${effects[m].name}`;
            }
        }
        html += '</ul>';
        ChatMessage.create({
            user: game.userId,
            content: html
        });
    }
}

// Start of Turn
// At the end of any combatants turn, loop over all effects of all combatants
// and if our token at the moment is the originator, process the effects and
// possibly remove it.
async function startOfTurn(token) {
    log(`---- Start of ${token.name}'s turn ----`);
    let tokens = [];
    for (let i = 0; i < game.combat.combatants.contents.length; i++) {
        tokens.push(game.combat.combatants.contents[i].tokenId);
    }
    for (let j = 0; j < tokens.length; j++) {
        let reviewToken = canvas.tokens.placeables.find(x => x.id === tokens[j]);
        log(`-- Review ${reviewToken.name}'s effects --`);
        let effects = reviewToken.actor.getEmbeddedCollection("ActiveEffect");
        for (let m = 0; m < effects.contents.length; m++) {
            let effect = effects.contents[m];
            log(`Effect: ${effect.name}`);
            if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
                // Process effect
                if (effect.getFlag('csm-lancer-qol', 'duration.label').includes('start') &&
                    effect.getFlag('csm-lancer-qol', 'originID') === token.document._id) {
                    log(`We should update event ${effect.name} on ${token.name}`);
                    let turns = effect.getFlag('csm-lancer-qol', 'duration.turns');
                    if (turns > 0) {
                        turns--;
                        await effect.setFlag('csm-lancer-qol', 'duration.turns', turns);
                    } else {
                        log(`${effect.name} is at ${turns} turns already.`);
                    }
                }
            } else {
                log(`${effect.name} on ${reviewToken.name} is not a timed effect.`);
            }
        }
        let removeIds = reviewToken.actor.effects.filter(x =>
            x.getFlag('csm-lancer-qol', 'duration.turns') <= 0 &&
            x.getFlag('csm-lancer-qol', 'duration.rounds') <= 0 &&
            x.getFlag('csm-lancer-qol', 'duration.label').includes('start')
        );
        for (let n = 0; n < removeIds.length; n++) {
            await reviewToken.actor.deleteEmbeddedDocuments("ActiveEffect", [removeIds[n]._id]);
        }
    }
}

// End of Turn
// At the end of any combatants turn, loop over all effects of all combatants
// and if our token at the moment is the originator, process the effects and
// possibly remove it.
async function endOfTurn(token) {
    log(`---- End of ${token.name}'s turn ----`);
    let tokens = [];
    for (let i = 0; i < game.combat.combatants.contents.length; i++) {
        tokens.push(game.combat.combatants.contents[i].tokenId);
    }
    for (let j = 0; j < tokens.length; j++) {
        let reviewToken = canvas.tokens.placeables.find(x => x.id === tokens[j]);
        log(`-- Review ${reviewToken.name}'s effects --`);
        let effects = reviewToken.actor.getEmbeddedCollection("ActiveEffect");
        for (let m = 0; m < effects.contents.length; m++) {
            let effect = effects.contents[m];
            log(`Effect: ${effect.name}`);
            if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
                // Process effect
                if (effect.getFlag('csm-lancer-qol', 'duration.label').includes('end') &&
                    effect.getFlag('csm-lancer-qol', 'originID') === token.document._id) {
                    log(`We should update event ${effect.name} on ${token.name}`);
                    let turns = effect.getFlag('csm-lancer-qol', 'duration.turns');
                    if (turns > 0) {
                        turns--;
                        await effect.setFlag('csm-lancer-qol', 'duration.turns', turns);
                    } else {
                        log(`${effect.name} is at ${turns} turns already.`);
                    }
                }
            } else {
                log(`${effect.name} on ${reviewToken.name} is not a timed effect.`);
            }
        }
        let removeIds = reviewToken.actor.effects.filter(x =>
            x.getFlag('csm-lancer-qol', 'duration.turns') <= 0 &&
            x.getFlag('csm-lancer-qol', 'duration.rounds') <= 0 &&
            x.getFlag('csm-lancer-qol', 'duration.label').includes('end')
        );
        for (let n = 0; n < removeIds.length; n++) {
            await reviewToken.actor.deleteEmbeddedDocuments("ActiveEffect", [removeIds[n]._id]);
        }
    }
}

// End of a Round
// 1. Decrement all effect round counters by 1
async function endOfRound(combat) {
    let tokens = [];
    for (let i = 0; i < combat.combatants.contents.length; i++) {
        tokens.push(combat.combatants.contents[i].tokenId);
    }
    for (let j = 0; j < tokens.length; j++) {
        let token = canvas.tokens.placeables.find(x => x.id === tokens[j]);
        let effects = token.actor.getEmbeddedCollection("ActiveEffect");
        for (let m = 0; m < effects.contents.length; m++) {
            let effect = effects.contents[m];
            if (typeof effect.getFlag('csm-lancer-qol', 'duration') !== 'undefined') {
                let rounds = effect.getFlag('csm-lancer-qol', 'duration.rounds');
                if (rounds > 0) {
                    rounds--;
                    await effect.setFlag('csm-lancer-qol', 'duration.rounds', rounds);
                } else {
                    log(`${effect.name} is at ${rounds} rounds already.`);
                }
            } else {
                log(`${effect.name} is not a timed effect.`);
            }
        }
    }
    return tokens;
}

export async function combatTracking(combat, changed, options, user) {
    if (game.settings.get('csm-lancer-qol', 'effectsTimer')) {
        log('**updateCombat**');
        // log(combat);
        if (combat.current.combatantId !== null) { // Turn start
            let currentCombatant = combat.combatants.find(x => x._id === combat.current.combatantId);
            let currentActor = game.actors.find(x => x.id === currentCombatant.actorId);
            let currentToken = canvas.tokens.placeables.find(x => x.id === currentCombatant.tokenId);
            log(`Current Turn Actor: ${currentActor.name}`);
            log(`Current Turn Token: ${currentToken.name}`);
            if (game.user.isGM) {
                startOfTurn(currentToken);
                effectsReport(currentToken);
            }
        }
        if (combat.previous.combatantId !== null) { // Turn end
            let previousCombatant = combat.combatants.find(x => x._id === combat.previous.combatantId);
            let previousActor = game.actors.find(x => x.id === previousCombatant.actorId);
            let previousToken = canvas.tokens.placeables.find(x => x.id === previousCombatant.tokenId);
            log(`Previous Turn Actor: ${previousActor.name}`);
            log(`Previous Turn Token: ${previousToken.name}`);
            if (game.user.isGM) {
                endOfTurn(previousToken);
            }
        }
        if (changed.round) {
            log(`ROUND ${changed.round}! FIGHT!`);
            if (game.user.isGM) {
                log(await endOfRound(combat));
            }
        }
        // log(changed);
        // log(options);
        // log(user);
    }
}
